﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

	public float speed = 10f;
	public GameObject obstacleObject;

	void Update () {
		if(networkView.isMine)
		{
			InputMovement();
			Camera.main.GetComponent<cameraController>().target = rigidbody.position;
		}
	}

	void InputMovement()
	{
		if (Input.GetKey (KeyCode.W))
			rigidbody.MovePosition (rigidbody.position + Vector3.forward * speed * Time.deltaTime);
		if (Input.GetKey (KeyCode.S))
			rigidbody.MovePosition (rigidbody.position - Vector3.forward * speed * Time.deltaTime);
		if (Input.GetKey (KeyCode.D))
			rigidbody.MovePosition (rigidbody.position + Vector3.right * speed * Time.deltaTime);
		if (Input.GetKey (KeyCode.A))
			rigidbody.MovePosition (rigidbody.position - Vector3.right * speed * Time.deltaTime);

	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "obsCube")
		{
			other.gameObject.SetActive(false);
			Network.Instantiate(obstacleObject, new Vector3(-24.0f, 1.0f, 0.0f), Quaternion.identity, 0);
		}
	}
}
