﻿using UnityEngine;
using System.Collections;

public class cameraController : MonoBehaviour {

	private Vector3 offset;
	public Vector3 target;

	// Use this for initialization
	void Start () {
		offset = transform.position;
		transform.Rotate (new Vector3 (15.0f, 0.0f, 0.0f));
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = offset + target;
	}
}
