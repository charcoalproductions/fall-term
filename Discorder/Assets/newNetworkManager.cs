﻿using UnityEngine;
using System.Collections;

public class newNetworkManager : MonoBehaviour {

	private const string typeName = "Discorder";
	private const string gameName = "Discorder 1";

	private void StartServer()
	{
		Network.InitializeServer(2, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost (typeName, gameName);
	}
}
